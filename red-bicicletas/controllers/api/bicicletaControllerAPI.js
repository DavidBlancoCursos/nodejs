var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = function(req, res){
    Bicicleta.find({},function(err, bicicletas){
        res.status(200).json({
            bicicletas: bicicletas
        });
    });   
}

exports.bicicleta_created = function(req, res){
    var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_update = function(req, res){
    
    Bicicleta.findByCode(req.body.code, function(error, bici){
        
        //bici.code = req.body.code;
        bici.color = req.body.color;
        bici.modelo  = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];
        
        Bicicleta.update(bici, (err, raw) => {

            res.status(200).json({
                bicicleta: bici
            });

        });
    }); 
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code, (err, raw) => {
        
        res.status(204).send();

    });
    
}
/*
//API sin mongoose

var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas : Bicicleta.allBicis
    });
}

exports.bicicleta_created = function(req,res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat,req.body.lng];
    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta : bici
    });
}

exports.bicicleta_update = function(req,res){
    

    var bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo; 
    bici.ubicacion = [req.body.lat,req.body.lng];

    res.status(200).json({
        bicicleta : bici
    });
}


exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);

    res.status(204).send();
}
*/