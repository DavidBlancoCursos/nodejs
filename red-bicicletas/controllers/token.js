var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next){

        console.log("Recogemos el token por parametro : "+ req.params.token);
        
        Token.findOne({ token: req.params.token}, function(err, token) {
            if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No encontramos ningun usuario con este token o haya expirado'});
            Usuario.findById(token._userId, function(err, usuario){
                if (!usuario) return res.status(400).send({ msg: 'No encontramos un usuario con ese token'});
                
                if (usuario.verificado) return res.redirect('/usuarios');

                console.log("");
                

                usuario.verificado = true;
                usuario.save(function (err){
                    if (err) { return res.status(500).send({ msg: err.message });}
                    res.redirect('/');
                });
            });
        });

        
    }
}