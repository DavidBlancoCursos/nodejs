var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

const bcrypt = require('bcrypt');

const saltRounds = 10;

const crypto = require('crypto');
const mailer = require('../mailer/mailer');
const Token = require('../models/token');

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)+@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre : {
        type : String,
        tirm: true,
        required : [true, 'El nombre es obligatorio']
    },
    email : { 
        type : String,
        trim : true,
        required: [true , 'Email obligatorio'],
        lowercase: true,
        unique : true,
        validate : [validateEmail, 'Ingrese un email valido'],
        match : [/^\w+([\.-]?\w+)+@\w+([\.-]?\w+)*(\.\w{2,3})+$/]

    },
    password : { 
        type : String,
        required: [true , 'Password obligatoria']
    },
    passwordResetToken : String,
    passwordResetTokenExpires : Date,
    verificado : {
        type : Boolean,
        default : false
    }
})

usuarioSchema.plugin(uniqueValidator, { message : 'El {PATH} ya existe con otro usuario.' });

//Antes de que hagamos un sava vamos a ejecutar la funcion que definamos.
usuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({ usuario : this._id, bicicleta : biciId, desde : desde , hasta : hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});

    console.log("Mi token : "+token);
    

    var email_destination = this.email;
    token.save(function(err){
        if (err) { return console.log("Error al salvar el token :" + err.message);}

        var mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, haga click en este link para verificar su ceunta: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return console.log("Error al enviar el mail :" + err.message);}

            console.log('Se ha enviado un email de verificacion a : '+ email_destination + '.');
        });
    });
}

usuarioSchema.methods.resetPassword = function (cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return cb (err); }

        var mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola,\n\n' + 'Por favor, haga click en este link para resetear su password : \n' 
            + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return cb(err);}

            console.log('Se ha enviado un email para resetar la password a  : '+ email_destination + '.');
        });

        cb(null);
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);